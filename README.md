# Atollgen project, documentation generation repository

This repository contains the documentation generation for the Atollgen project.

The documentation is generated using
[Sphinx](http://www.sphinx-doc.org/en/master/) via the gitlab CI.

The documentation is available at https://atollgen.gitlab.io/docs/
