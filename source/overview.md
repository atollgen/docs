# Overview

The Atollgen project consist in several tools that aim characterization of
mobile genomic islands. The basic requirement is that the host is available in
the NCBI (with a known **accession number**) and the island bound is known.

The project include several modules :


- Atollgen CLI, that, given an accession number and an island bound, will run
  multiple analysis to characterize the island
- Atollgen pipeline that provide notebooks to ensure experiment reproducibility
  for an ongoing paper, including pre-processing of heterogeneous databases of
  genomic islands.
- Entrez Fetcher, a helper tool that is built and improve on the NCBI Entrez API
  and allow batch download of docsum and genome sequences from the NCBI

Each of these modules are described in their respective pages.

Atollgen CLI is what you are looking at if you want to quickly annotate one (of
several) islands. It outputs one json file per island, including all the
analysis output.

Atollgen Pipeline is what you are looking at if you want to reproduce A. Bioteau
ongoing work on Mobile Genomic Island characterization.
