.. Atollgen documentation master file, created by
   sphinx-quickstart on Wed Dec 28 11:42:23 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AtollGen's documentation!
====================================

.. include:: overview.md
   :parser: myst_parser.sphinx_


.. toctree::
   :caption: Contents:


   modules/cli.md
   modules/pipeline.md
   modules/efetcher.md

.. toctree::
   :caption: API:

   Atollgen CLI API <https://atollgen.gitlab.io/atollgen-cli>
   Entrez Fetcher API <https://atollgen.gitlab.io/entrez-fetcher/entrez_fetcher.html>



Documentation's indices and tables
==================================

* :ref:`search`
