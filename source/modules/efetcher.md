# Entrez Fetcher

Fetch data from the entrez e-utilities. Provide clever batching, client-side
rate limit (and automatic retry if even the rate limitation fail, as the entrez
api can be more conservative than what is announced).

[The API documentation live here](https://atollgen.gitlab.io/entrez-fetcher/entrez_fetcher.html)
